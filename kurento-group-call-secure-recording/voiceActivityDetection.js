var fs = require('fs');
var wav = require('node-wav');

function getSignalAverageEnergy(signal)
{
    var signalTotalEnergy = 0;
    const signalLength = signal.length;

    signal.forEach(function(value){
        signalTotalEnergy += Math.pow(value, 2);
    });

    return signalTotalEnergy / signalLength;
}

function getFrameRateAndSignal(audioFile)
{
    var buffer = fs.readFileSync(audioFile);
    var result = wav.decode(buffer);

    var signal = result.channelData[0];
    var frameRate =  result.sampleRate;

    return { frameRate: frameRate, signal: signal };
}

function writeSegmentFile(segments, segmentFile)
{
    if (segments.length == 0)
    {
        return;
    }

    const segmentString = segments.join('\n');
    fs.writeFileSync(segmentFile, segmentString);
}

function writeAudioFile(frameRate, signal, file)
{
    var channelData = [];
    channelData.push(signal);
    var buffer = wav.encode(channelData, { sampleRate: frameRate, float: true, bitDepth: 32 });

    fs.writeFileSync(file, buffer);
}

function float32ArrayConcat(first, second)
{
    var firstLength = first.length;
    var secondLength = second.length;
    var result = new Float32Array(firstLength + secondLength);
    result.set(first);
    result.set(second, firstLength);

    return result;
}

function voiceActivityDetection(originalFrameRate, originalSignal, percEnergyThreshold) {
    const windowDurationInSeconds = 0.2;
    const windowShiftInSeconds = windowDurationInSeconds;

    const originalSignalLength = originalSignal.length;
    var returnSignal = [];
    var result = [];
    const windowLength = Math.floor(windowDurationInSeconds * originalFrameRate);
    const windowShiftLength = Math.floor(windowShiftInSeconds * originalFrameRate);

    var i = 0;

    const averageOriginalSignalEnergy = getSignalAverageEnergy(originalSignal);
    var voiceActivityStart = -1;

    while (i < originalSignalLength) {
        if (originalSignalLength < windowLength) {
            break;
        }

        const subSignal = originalSignal.slice(i, i + windowLength);
        const averageSubSignalEnergy = getSignalAverageEnergy(subSignal);

        if (averageSubSignalEnergy < (averageOriginalSignalEnergy * percEnergyThreshold)) {
            if (voiceActivityStart >= 0) {
                result = result.concat([voiceActivityStart, i]);
                returnSignal = float32ArrayConcat(returnSignal, originalSignal.slice(voiceActivityStart, i));
                voiceActivityStart = -1;
            }

            i += windowLength;
        }

        else {
            if (voiceActivityStart == -1) {
                voiceActivityStart = i;
            }

            i += windowShiftLength;
        }
    }

    if (voiceActivityStart >= 0) {
        result = result.concat([voiceActivityStart, originalSignalLength]);
        returnSignal = float32ArrayConcat(returnSignal, originalSignal.slice(voiceActivityStart, originalSignalLength));
    }

    return {segments: result, frameRate: originalFrameRate, signal: returnSignal};
}

// Reads the original file and returns the voice activity frames
function voiceActivityDetectionTask(originalAudioFile, newAudioFile, segmentFile) {
    const originalFrameRateAndSignal = getFrameRateAndSignal(originalAudioFile);
    const voiceActivityDetectionResult = voiceActivityDetection(originalFrameRateAndSignal.frameRate,
        originalFrameRateAndSignal.signal, 0.01);

    writeSegmentFile(voiceActivityDetectionResult.segments, segmentFile);

    writeAudioFile(voiceActivityDetectionResult.frameRate, voiceActivityDetectionResult.signal, newAudioFile);
}

function doVoiceActivityDetection(inputAudioFile, segmentFile, newAudioFile) {
    voiceActivityDetectionTask(inputAudioFile, newAudioFile, segmentFile);
}

module.exports = doVoiceActivityDetection;