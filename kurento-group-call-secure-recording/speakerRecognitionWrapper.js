const speakerRecognitionScript = "speaker-recognition/src/speaker-recognition.py";

module.exports = {
    enroll: function(enrollmentFolder, modelFilename)
    {
	const execSync = require('child_process').execSync;
	var enrollmentCommand = "python " + speakerRecognitionScript + " -t enroll -i \"" + enrollmentFolder + "/person-*\"" + " -m \"" + modelFilename + "\""; 

	var executionCode = execSync(enrollmentCommand);
	console.log("output for enrollment command: ", executionCode);
    },

    predict: function (modelFilename, audioFilename, segmentFilename, outputFilename)
    {
	const execSync = require('child_process').execSync;
	var predictionCommand = "python " + speakerRecognitionScript + " -t predict -i \"" + audioFilename + "\"" + " -m \"" + modelFilename + "\" -s \"" + segmentFilename + "\" -o \"" + outputFilename + "\"";

	var executionCode = execSync(predictionCommand);
	console.log("output for prediction command: ", executionCode);
    }
};


    
