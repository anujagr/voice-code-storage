var path = require('path');
const transcriptionScript = "transcribe_async.py";
process.env.GOOGLE_APPLICATION_CREDENTIALS = path.resolve("keys/TranscriptionKey.json");
// process.env.PATH += ";C:\\Programs\\ffmpeg-20170208-3aae1ef-win64-static\\bin";

function convertToWavFormat(originalRecordingFilename, recordingInWavFilename)
{
    const execFileSync = require('child_process').execFileSync;
    const commandOutput = execFileSync('ffmpeg', ['-v', 'error', '-i', originalRecordingFilename, '-ar', 16000, '-ac', 1, recordingInWavFilename]);
}

function doVoiceActivityDetection(recordingFilename, segmentFilename, noVoiceFilename)
{
    var voiceActivityDetection = require('./voiceActivityDetection');
    voiceActivityDetection(recordingFilename, segmentFilename, noVoiceFilename);
}

function doTranscription(recordingFilename, segmentFilename, transcriptionFilename)
{
    const execFileSync = require('child_process').execFileSync;

    const commandOutput = execFileSync('python', [transcriptionScript, recordingFilename, segmentFilename, transcriptionFilename]);
}

function getTranscriptionFilenames(recordingFullPath)
{
    var recordingFolder = path.dirname(recordingFullPath);
    var recordingNameWithoutExt = path.basename(recordingFullPath, path.extname(recordingFullPath));
    var recordingInWavFilename = path.join(recordingFolder, recordingNameWithoutExt + ".wav");
    var segmentFilename = path.join(recordingFolder, recordingNameWithoutExt + "Segment.txt");
    var recordingWithoutSilenceName = path.join(recordingFolder, recordingNameWithoutExt + "OnlyVoice.wav");

    var transcriptionFilename = path.join(recordingFolder, recordingNameWithoutExt + "Transcribe.txt");
    var speakerRecognitionModelFilename = path.join(recordingFolder, recordingNameWithoutExt + "Model.out");
    var segmentAndSpeakerFilename = path.join(recordingFolder, recordingNameWithoutExt + "SegmentWithSpeaker.txt");

    return {
        recordingInWavFilename : recordingInWavFilename,
        segmentFilename : segmentFilename,
        recordingWithoutSilenceName : recordingWithoutSilenceName,
        transcriptionFilename : transcriptionFilename,
	speakerRecognitionModelFilename: speakerRecognitionModelFilename,
	segmentAndSpeakerFilename: segmentAndSpeakerFilename
    };
}

function doSpeakerRecognition(audioFilename, segmentFilename, modelFilename, outputFilename, enrollmentFolder)
{
    console.log("Doing enrollment");

    var speakerRecognitionWrapper = require('./speakerRecognitionWrapper');
    speakerRecognitionWrapper.enroll(enrollmentFolder, modelFilename);

    console.log("Doing prediction");

    speakerRecognitionWrapper.predict(modelFilename, audioFilename, segmentFilename, outputFilename);
}

function convertAndTranscribeAudio(recordingFullPath, enrollmentFolder)
{
    transcriptionFilenames = getTranscriptionFilenames(recordingFullPath);

    console.log("Converting .webm file to .wav file");
    convertToWavFormat(recordingFullPath, transcriptionFilenames.recordingInWavFilename);

    console.log("Doing voice activity detection for audio files");

    doVoiceActivityDetection(transcriptionFilenames.recordingInWavFilename,
                             transcriptionFilenames.segmentFilename,
                             transcriptionFilenames.recordingWithoutSilenceName);

    console.log("Doing speaker recognition for audio file");

    doSpeakerRecognition(transcriptionFilenames.recordingInWavFilename,
			 transcriptionFilenames.segmentFilename,
			 transcriptionFilenames.speakerRecognitionModelFilename,
			 transcriptionFilenames.segmentAndSpeakerFilename,
			 enrollmentFolder);

    console.log("Calling google api to do transcription for audio files");

    doTranscription(transcriptionFilenames.recordingInWavFilename,
                    transcriptionFilenames.segmentAndSpeakerFilename,
                    transcriptionFilenames.transcriptionFilename);
}

const recordingFullPath = process.argv[2].replace("file://", "");
//const enrollmentFolder = process.argv[3].replace("file://", "");
const enrollmentFolder = "/opt/vagnu/speaker-recognition/src/tmp";

convertAndTranscribeAudio(recordingFullPath, enrollmentFolder);
