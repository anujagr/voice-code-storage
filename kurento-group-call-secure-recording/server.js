/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 */
var UserRegistry = require('./user-registry.js');
var UserSession = require('./user-session.js');

// store global variables
var userRegistry = new UserRegistry();
var rooms = {};
var idCounter = 0;

var express = require('express');

// kurento required
var path = require('path');
var url = require('url');
var kurento = require('kurento-client');
//var recordingDirectory = path.resolve("recordings"); // use this path while running on windows
var recordingDirectory = '/tmp/recordings';

// Constants
var settings = {
        WEBSOCKETURL: "https://localhost:8080/",
        KURENTOURL: "ws://localhost:8888/kurento",
        RECORDINGURL: 'file://' + recordingDirectory + '/',
        RECORDINGEXT: '.webm'
};

// Singleton Kurento Client, gets set on first interaction
var kurentoClient = null;

// Server setup variables
var ws = require('ws');
var fs    = require('fs');
var https = require('https');
var options =
    {
        key:  fs.readFileSync('keys/server.key'),
        cert: fs.readFileSync('keys/server.crt')
    };

/*
 * Server startup
 */
var app = express();
var asUrl = url.parse(settings.WEBSOCKETURL);
var port = asUrl.port;
var server = https.createServer(options, app).listen(port, function() {
    console.log('Kurento Tutorial started');
    console.log('Open ' + url.format(asUrl) + ' with a WebRTC capable browser');
});

var expressWs = require('express-ws')(app, server);

function nextUniqueId() {
    idCounter++;
    return idCounter.toString();
}

/**
 * Message handlers
 */
app.ws('/groupcall', function (ws, req) {
    var sessionId = nextUniqueId();
    var userList = '';
    for (var userId in userRegistry.usersById) {
        userList += ' ' + userId + ',';
    }
    console.log('receive new client with id: ' + sessionId + ' currently have : ' + userList);

    ws.on('error', function (data) {
        console.log('Connection: ' + sessionId + ' error : ' + data);
        leaveRoom(sessionId, function () {

        });
    });

    ws.on('close', function () {
        console.log('Connection: ' + sessionId + ' closed');
        leaveRoom(sessionId, function () {
            var userSession = userRegistry.getById(sessionId);
            stop(userSession.id);
        });
    });

    ws.on('message', function (inputMessage) {
        var message = JSON.parse(inputMessage);

        console.log('Connection: ' + sessionId + ' receive message: ' + message.id);

        switch (message.id) {
            case 'register':
                console.log('registering ' + sessionId);
                register(sessionId, message.name, ws, function(){

                });

                break;
            case 'joinRoom':
                console.log(sessionId + ' joinRoom : ' + message.roomName);
                joinRoom(sessionId, message.roomName, function (error) {
                    if (error) {
                        console.error("Not able to join room with name: %s, for user: %d", message.roomName, sessionId);
                    }
                });
                break;
            case 'receiveVideoFrom':
                console.log(sessionId + ' receiveVideoFrom : ' + message.sender);
                receiveVideoFrom(sessionId, message.sender, message.sdpOffer, function () {

                });
                break;
            case 'leaveRoom':
                console.log(sessionId + ' leaveRoom');
                leaveRoom(sessionId);
                break;
            case 'call':
                console.log("Calling");
                call(sessionId, message.to, message.from, message.roomName);
                break;
            case 'onIceCandidate':
                addIceCandidate(sessionId, message);
                break;
            default:
                ws.send(JSON.stringify({
                    id : 'error',
                    message : 'Invalid message ' + message
                }));
                break;
        }
    });
});

/**
 * Register user to server
 * @param sessionId
 * @param name
 * @param callback
 */
function register(sessionId, name, ws, callback){
    var userSession = new UserSession(sessionId, ws);
    userSession.name = name;
    userRegistry.register(userSession);
    userSession.sendMessage({
        id: 'registered',
        sessionId : sessionId,
        roomName: generateUUID(),
        data: 'Successfully registered ' + sessionId
    });
    console.log(userRegistry);
}

/**
 * Gets and joins room
 * @param sessionId
 * @param roomName
 * @param callback
 */
function joinRoom(sessionId, roomName, callback) {
    getRoom(roomName, function (error, room) {
        if (error) {
            console.error("Error while getting room: ", error);
            callback(error)
        }
        console.log("Got room with name: ", room.name);

        join(sessionId, room, function (error, user) {
            if (error)
            {
                console.error("Error while joining room: ", error);
                callback(error);
            }
            console.log('join success for user: %d, in room: %s',user.id, user.roomName);
        });
    });

    callback(null);
}

/**
 * Gets room. Creates room if room does not exist
 * @param roomName
 * @param callback
 */
function getRoom(roomName, callback) {

    var room = rooms[roomName];

    if (room == null) {
        console.log('create new room : ' + roomName);
        getKurentoClient(function (error, kurentoClient) {
            if (error) {
                console.error("Error while getting kurento client: ", error);
                return callback(error);
            }

            // create pipeline for room
            kurentoClient.create('MediaPipeline', function (error, pipeline) {
                if (error) {
                    console.error("Error while creating media pipeline: ", error);
                    return callback(error);
                }

                room = {
                    name: roomName,
                    pipeline: pipeline,
                    participants: {}
                };
                rooms[roomName] = room;
                callback(null, room);
            });
        });
    } else {
        console.log('get existing room : ' + roomName);
        callback(null, room);
    }
}

/**
 * Join (conference) call room
 * @param sessionId
 * @param room
 * @param callback
 */
function join(sessionId, room, callback) {
    // create user session
    var userSession = userRegistry.getById(sessionId);
    userSession.setRoomName(room.name);

    room.pipeline.create('WebRtcEndpoint', function (error, outgoingMedia) {
        if (error) {
            console.error('no participant in room');
            // no participants in room yet release pipeline
            if (Object.keys(room.participants).length == 0) {
                room.pipeline.release();
            }
            return callback(error);
        }
        outgoingMedia.setMaxVideoSendBandwidth(30);
        outgoingMedia.setMinVideoSendBandwidth(20);
        userSession.outgoingMedia = outgoingMedia;

        // add ice candidate the get sent before endpoint is established
        var iceCandidateQueue = userSession.iceCandidateQueue[sessionId];
        if (iceCandidateQueue) {
            while (iceCandidateQueue.length) {
                var message = iceCandidateQueue.shift();
                console.error('user : ' + userSession.id + ' collect candidate for outgoing media');
                userSession.outgoingMedia.addIceCandidate(message.candidate);
            }
        }

        userSession.outgoingMedia.on('OnIceCandidate', function (event) {
            console.log("generate outgoing candidate : " + userSession.id);
            var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
            userSession.sendMessage({
                id: 'iceCandidate',
                sessionId: userSession.id,
                candidate: candidate
            });
        });

        // Create and start recording.
        var currentTimeAsString = new Date().toISOString().replace(/[^a-z0-9]/gi, '_');
        var recordingName = settings.RECORDINGURL + room.name + "/" + userSession.name + "-" + currentTimeAsString + settings.RECORDINGEXT;
        userSession.recordingPath = recordingName;

        room.pipeline.create('RecorderEndpoint', {'uri': recordingName, 'mediaProfile': 'WEBM_AUDIO_ONLY'}, function (error, recorder)
        {
            if (error) {
                console.error("Error while creating recording: ", error);
                return callback(error);
            }

            console.log("Recording created for user with name: ", userSession.name);
            userSession.recorder = recorder;

            userSession.outgoingMedia.connect(recorder, function(error)
            {
                if (error)
                {
                    console.error("Not able to connect recording for user with name: " + userSession.name + " with error: " + error);
                    return callback(error);
                }

                console.log("Recording connected for user with name: ", userSession.name);

                userSession.recorder.record(function(){
                    console.log("Recording started for user with name: ", userSession.name);
                });
            });
        });

        // notify other user that new user is joining
        var usersInRoom = room.participants;
        var data = {
            id: 'newParticipantArrived',
            new_user_id: userSession.id
        };

        // notify existing user
        for (var i in usersInRoom) {
            usersInRoom[i].sendMessage(data);
        }

        var existingUserIds = [];
        for (var i in room.participants) {
            existingUserIds.push(usersInRoom[i].id);
        }
        // send list of current user in the room to current participant
        userSession.sendMessage({
            id: 'existingParticipants',
            data: existingUserIds,
            roomName: room.name
        });

        // register user to room
        room.participants[userSession.id] = userSession;

        callback(null, userSession);
    });
}

/**
 * Leave (conference) call room
 * @param sessionId
 * @param callback
 */
function leaveRoom(sessionId, callback) {
    var userSession = userRegistry.getById(sessionId);

    if (!userSession) {
        return;
    }

    var room = rooms[userSession.roomName];

    if(!room){
        return;
    }

    if (userSession.recorder)
    {
        console.log("stopping the recording for user with name: ", userSession.name);
        userSession.recorder.stop();
        delete userSession.recorder;

        transcribeAudio(userSession.recordingPath, userSession);
        delete userSession.recordingPath;
    }

    console.log('notify all user that ' + userSession.id + ' is leaving the room ' + room.name);
    var usersInRoom = room.participants;
    delete usersInRoom[userSession.id];
    userSession.outgoingMedia.release();
    // release incoming media for the leaving user
    for (var i in userSession.incomingMedia) {
        userSession.incomingMedia[i].release();
        delete userSession.incomingMedia[i];
    }

    var data = {
        id: 'participantLeft',
        sessionId: userSession.id
    };
    for (var  i in usersInRoom) {
        var user = usersInRoom[i];
        // release viewer from this
        user.incomingMedia[userSession.id].release();
        delete user.incomingMedia[userSession.id];

        // notify all user in the room
        user.sendMessage(data);
    }

    delete userSession.roomName;
}

function readTranscript(filename)
{
    var realFilename = filename.replace("file://", "");
    var recordingFolder = path.dirname(realFilename);
    var realFileNameWithoutExt = path.basename(realFilename, path.extname(realFilename));
    var transcriptionFilename = path.join(recordingFolder, realFileNameWithoutExt + "Transcribe.txt");

    console.log("start reading transcript");

    var fs = require('fs');
    var transcript;

    try {
        transcript = fs.readFileSync(transcriptionFilename);
    }
    catch(err) {
        transcript = "Error " + err  + " while reading the transcript file" + transcriptionFilename;
    }

    return transcript;
}

function transcribeAudio(recordingFullPath, userSession)
{
    var roomName = userSession.roomName;

    // Spawning a child process to do transcription in order to unblock the current process to receive requests.

    console.log("Audio transcription started");

    var child_process= require('child_process');
    var child = child_process.spawn('node', ['transcribeAudio.js', recordingFullPath]);

    child.stdout.on('data', function(buf) {
        console.log('%s', String(buf));
    });
    child.stderr.on('data', function(buf) {
        console.error('%s', String(buf));
    });
    child.on('close', function(code) {
        console.log('Child process exited with code : ', code);

        var transcript = readTranscript(recordingFullPath).toString();
        console.log("Transcript before sending: ", transcript);

        var transcriptData = {
            id: 'transcript',
            roomName: roomName,
            transcript: transcript
        };

        userSession.sendMessage(transcriptData);
    });
}


/**
 * Unregister user
 * @param sessionId
 */
function stop(sessionId) {
    userRegistry.unregister(sessionId);
}

/**
 * Invite other user to a (conference) call
 * @param callerId
 * @param to
 * @param from
 */
function call(callerId, to, from, incomingRoomName) {
    if(to === from){
        return;
    }
    var roomName;
    var caller = userRegistry.getById(callerId);
    var rejectCause = 'User ' + to + ' is not registered';
    if (userRegistry.getByName(to)) {
        if(!caller.roomName){
            roomName = incomingRoomName;
            joinRoom(caller.id, roomName, function(error){
                if (error) {
                    console.error("Not able to join room: ", error);
                    rejectCause = "Not able to create/join room";
                }

                console.log("Successfully created and joined room");
            });
        }
        else{
            roomName = caller.roomName;
        }

        if (roomName) {
            var callee = userRegistry.getByName(to);
            callee.peer = from;
            caller.peer = to;
            var message = {
                id: 'incomingCall',
                from: from,
                roomName: roomName
            };
            try {
                return callee.sendMessage(message);
            } catch (exception) {
                rejectCause = "Error " + exception;
            }
        }
    }
    var message  = {
        id: 'callResponse',
        response: 'rejected: ',
        message: rejectCause
    };
    caller.sendMessage(message);
}

/**
 * Retrieve sdpOffer from other user, required for WebRTC calls
 * @param sessionId
 * @param senderId
 * @param sdpOffer
 * @param callback
 */
function receiveVideoFrom(sessionId, senderId, sdpOffer, callback) {
    var userSession = userRegistry.getById(sessionId);
    var sender = userRegistry.getById(senderId);

    getEndpointForUser(userSession, sender, function (error, endpoint) {
        if (error) {
            callback(error);
        }

        endpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
            console.log("process offer from : " + senderId + " to " + userSession.id);
            if (error) {
                return callback(error);
            }
            var data = {
                id: 'receiveVideoAnswer',
                sessionId: sender.id,
                sdpAnswer: sdpAnswer
            };
            userSession.sendMessage(data);

            endpoint.gatherCandidates(function (error) {
                if (error) {
                    return callback(error);
                }
            });
            return callback(null, sdpAnswer);
        });
    });
}

/**
 * Get user WebRTCEndPoint, Required for WebRTC calls
 * @param userSession
 * @param sender
 * @param callback
 */
function getEndpointForUser(userSession, sender, callback) {
    // request for self media
    if (userSession.id === sender.id) {
        callback(null, userSession.outgoingMedia);
        return;
    }

    var incoming = userSession.incomingMedia[sender.id];
    if (incoming == null) {
        console.log('user : ' + userSession.id + ' create endpoint to receive video from : ' + sender.id);
        getRoom(userSession.roomName, function (error, room) {
            if (error) {
                return callback(error);
            }

            room.pipeline.create('WebRtcEndpoint', function (error, incomingMedia) {
                if (error) {
                    // no participants in room yet release pipeline
                    if (Object.keys(room.participants).length == 0) {
                        room.pipeline.release();
                    }
                    return callback(error);
                }
                console.log('user : ' + userSession.id + ' successfully created pipeline');
                incomingMedia.setMaxVideoSendBandwidth(30);
                incomingMedia.setMinVideoSendBandwidth(20);
                userSession.incomingMedia[sender.id] = incomingMedia;

                // add ice candidate the get sent before endpoint is established
                var iceCandidateQueue = userSession.iceCandidateQueue[sender.id];
                if (iceCandidateQueue) {
                    while (iceCandidateQueue.length) {
                        var message = iceCandidateQueue.shift();
                        console.log('user : ' + userSession.id + ' collect candidate for : ' + message.data.sender);
                        incomingMedia.addIceCandidate(message.candidate);
                    }
                }

                incomingMedia.on('OnIceCandidate', function (event) {
                    console.log("generate incoming media candidate : " + userSession.id + " from " + sender.id);
                    var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
                    userSession.sendMessage({
                        id: 'iceCandidate',
                        sessionId: sender.id,
                        candidate: candidate
                    });
                });
                sender.outgoingMedia.connect(incomingMedia, function (error) {
                    if (error) {
                        callback(error);
                    }
                    callback(null, incomingMedia);
                });

            });
        });
    } else {
        console.log('user : ' + userSession.id + ' get existing endpoint to receive video from : ' + sender.id);
        sender.outgoingMedia.connect(incoming, function (error) {
            if (error) {
                callback(error);
            }
            callback(null, incoming);
        });
    }
}

/**
 * Add ICE candidate, required for WebRTC calls
 * @param sessionId
 * @param message
 */
function addIceCandidate(sessionId, message) {
    var user = userRegistry.getById(sessionId);
    if (user != null) {
        // assign type to IceCandidate
        var candidate = kurento.register.complexTypes.IceCandidate(message.candidate);
        user.addIceCandidate(message, candidate);
    } else {
        console.error('ice candidate with no user receive : ' + sessionId);
    }
}

/**
 * Retrieve Kurento Client to connect to Kurento Media Server, required for WebRTC calls
 * @param callback
 * @returns {*}
 */
function getKurentoClient(callback) {
    if (kurentoClient !== null) {
        return callback(null, kurentoClient);
    }

    kurento(settings.KURENTOURL, function (error, _kurentoClient) {
        if (error) {
            var message = 'Coult not find media server at address ' + settings.KURENTOURL;
            return callback(message + ". Exiting with error " + error);
        }

        kurentoClient = _kurentoClient;
        callback(null, kurentoClient);
    });
}

/**
 * Generate unique ID, used for generating new rooms
 * @returns {string}
 */
function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}

app.use(express.static(path.join(__dirname, 'static')));
