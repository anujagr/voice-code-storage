#!/usr/bin/env python
# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Google Cloud Speech API sample application using the REST API for async
batch processing.

Example usage: python transcribe.py resources/audio.raw
"""

# [START import_libraries]
import argparse
import base64
import json
import time
import os

from googleapiclient import discovery
import httplib2
from oauth2client.client import GoogleCredentials
import scipy.io.wavfile as wavfile
from datetime import datetime
from datetime import timedelta

# [END import_libraries]


# Application default credentials provided by env variable
# GOOGLE_APPLICATION_CREDENTIALS
def get_speech_service():
    credentials = GoogleCredentials.get_application_default().create_scoped(
        ['https://www.googleapis.com/auth/cloud-platform'])
    http = httplib2.Http()
    credentials.authorize(http)

    return discovery.build('speech', 'v1beta1', http=http)

def create_and_send_requests(speech_file, segment_file, recording_start_time):
    recordingStartDate = datetime.strptime(recording_start_time, "%Y_%m_%dT%H_%M_%S_%fZ")
    fs, wavFileSignal = wavfile.read(speech_file)
    i = 0
    service = get_speech_service()
    service_requests_details = {}
    speaker_details = {}
    
    segments = open(segment_file, 'r').read().split('\n')
    segments = filter(None, segments)
    print len(segments)
    
    while i < len(segments):
        print i
        segStart = int(segments[i])
        segEnd = int(segments[i+1])
        username = segments[i+2]
        segStartTimeDeltaMilliseconds = (segStart * 1000) / fs
        segStartTime = recordingStartDate + timedelta(milliseconds=segStartTimeDeltaMilliseconds)
        i += 3
        buffered_audio_data = base64.b64encode(bytes(bytearray(wavFileSignal[segStart:segEnd])))
        
        service_request = service.speech().asyncrecognize(
            body={
                'config': {
                    # There are a bunch of config options you can specify. See
                    # https://goo.gl/KPZn97 for the full list.
                    'encoding': 'LINEAR16',  # raw 16-bit signed LE samples
                    'sampleRate': fs,  # 16 khz
                    # See http://g.co/cloud/speech/docs/languages for a list of
                    # supported languages.
                    'languageCode': 'en-IN',  # a BCP-47 language tag
                },
                'audio': {
                    'content': buffered_audio_data.decode('UTF- ')
                    }
                })
        response = service_request.execute()
        name = response['name']

        # Construct a GetOperation request.
        service_requests_details[segStartTime] = service.operations().get(name=name)
        speaker_details[segStartTime] = username
    
    return service_requests_details, speaker_details

def get_and_print_response(service_requests_details, speech_file, speaker_details, transcription_file):
    transcriptionFile = open(transcription_file, "w")
    
    for startTime, service_request in sorted(service_requests_details.iteritems()):
        while True:
            # Give the server a few seconds to process.
            print("waiting for response")
            time.sleep(1)
            # Get the long running operation with response.
            response = service_request.execute()

            if 'done' in response and response['done']:
                for result in response['response'].get('results', []):
                    for alternative in result['alternatives']:
                        user_name = speaker_details[startTime]
                        transcriptionFile.write(user_name + ", " + str(startTime) + ", " + alternative['transcript'] + "\n")
                        break
                        
                break
    
    transcriptionFile.close()

def GetRecordingStartTimeAndUser(speech_file):
    speech_file_name = os.path.basename(speech_file)
    speech_file_name_without_ext = os.path.splitext(speech_file_name)[0]
    user_name_and_time = speech_file_name_without_ext.split('-')
    
    return user_name_and_time[1], user_name_and_time[0]
    
def main(speech_file, segment_and_speaker_file, transcription_file):
    recording_start_time, registered_user_name = GetRecordingStartTimeAndUser(speech_file)
    
    service_requests_details, speaker_details = create_and_send_requests(speech_file, segment_and_speaker_file, recording_start_time)
    
    get_and_print_response(service_requests_details, speech_file, speaker_details, transcription_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        'speech_file', help='Full path of audio file to be recognized')
    parser.add_argument(
        'segment_and_speaker_file', help='Full path to segment and speaker file')
    parser.add_argument(
        'transcription_file', help='Full path to output transcription file')

    args = parser.parse_args()
    main(args.speech_file, args.segment_and_speaker_file, args.transcription_file)
