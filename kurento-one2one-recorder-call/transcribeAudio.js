var path = require('path');

function convertToWavFormat(filename, folder)
{
    // TODO: Make this independent of local machine directory structure.
    process.env.PATH += ";C:\\Programs\\ffmpeg-20170208-3aae1ef-win64-static\\bin";
    const execFileSync = require('child_process').execFileSync;
    var fullInputFilename = folder + "/" + filename;
    var outputfilename = folder + "/" + path.basename(filename, path.extname(filename)) +  ".wav";

    const commandOutput = execFileSync('ffmpeg', ['-v', 'error', '-i', fullInputFilename, '-ar', 16000, '-ac', 1, outputfilename]);
}

function doVoiceActivityDetection(recordingFolderPath)
{
    const execFileSync = require('child_process').execFileSync;

    // TODO: Make this independent of local machine directory structure.
    var pythonFilename = "C:\\vagnu\\product\\speakerSegmentation\\VAD.py";

    const commandOutput = execFileSync('python', [pythonFilename, recordingFolderPath]);
}

function doTranscription(file, recordingFolderName, recordingStartTime)
{
    const execFileSync = require('child_process').execFileSync;

    // TODO: Make this independent of local machine directory structure.
    var pythonFilename = "C:\\vagnu\\product\\api-client\\transcribe_async_modified.py";
    var fileNameWithoutExt = path.basename(file, path.extname(file));
    var audioFilename = recordingFolderName + "/" + fileNameWithoutExt +  ".wav";
    var segmentFilename = recordingFolderName + "/" + fileNameWithoutExt +  "Segment.txt";

    const commandOutput = execFileSync('python', [pythonFilename, audioFilename, segmentFilename, recordingStartTime, fileNameWithoutExt]);
    // console.log(commandOutput);
}

function readDirCallback(err, files)
{
    if( err ) {
        console.error( "Could not list the directory.", err );
        return;
    }

    console.log("Converting .webm files to .wav files");

    for (var i = 0; i < files.length; i++)
    {
        convertToWavFormat(files[i], recordingFolderName);
    }

    console.log("Doing voice activity detection for audio files");

    doVoiceActivityDetection(recordingFolderName);

    console.log("Calling google api to do transcription for audio files");

    for (var i = 0; i < files.length; i++)
    {
        doTranscription(files[i], recordingFolderName, recordingStartTime);
    }
}

// TODO: Make sure that recordingFolderName becomes system independent.
const recordingFolderName = process.argv[2].replace("file://", "C:");
const recordingStartTime = process.argv[3];

var fs = require('fs');
fs.readdir(recordingFolderName,
           readDirCallback.bind(
               {
                   recordingStartTime: recordingStartTime,
                   recordingFolderName: recordingFolderName
               })
);